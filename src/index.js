import React from 'react'
import ReactDOM from 'react-dom'
import 'core-js'
import { createRoute } from './config/routes'

import './style.module.scss'

if (process.env.NODE_ENV === 'development') {
  const axe = require('react-axe')
  axe(React, ReactDOM, 1000)
}

ReactDOM.render(createRoute(), document.getElementById('app'))
