import React from 'react'
import Provider from './Provider'
import store from './store'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import HomeContainer from '../containers/HomeContainer'

export const createRoute = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={HomeContainer} />
        <Route exact path="/home" component={HomeContainer} />
      </Switch>
    </Router>
  </Provider>
)
