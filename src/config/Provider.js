import React from 'react'
import { Provider } from 'react-redux'
import { any } from 'prop-types'

const ProviderWrapper = ({ children, store }) => (
  <Provider store={store}>{children}</Provider>
)

ProviderWrapper.propTypes = {
  children: any,
  store: any
}

export default ProviderWrapper
