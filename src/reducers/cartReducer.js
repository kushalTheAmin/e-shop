import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  items: [],
  cartLoading: false,
  cartError: false
}

const formSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    cartError: state =>
      Object.assign({}, state, {
        cartLoading: false,
        cartError: true
      }),
    cartReset: () => initialState
  }
})

const { actions, reducer } = formSlice

export const { cartError, cartReset } = actions

export default reducer
