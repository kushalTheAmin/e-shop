import { combineReducers } from 'redux'

import cartReducer from './cartReducer'

const reducers = {
  cart: cartReducer
}

const rootReducer = combineReducers(reducers)

export default rootReducer
