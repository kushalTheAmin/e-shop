import React, { Component } from 'react'

import style from './style.module.scss'

class HomeContainer extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className={style.homeContainer}>Welcome to E-Shop Home Page !</div>
    )
  }
}

export default HomeContainer
